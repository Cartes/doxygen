#include <stdlib.h>
#include <stdio.h>

int main() {


    int op1 = 5; /**< valor integer op1. */
    int op2 = 4; /**< valor integer op2. */

    int resultado = 0; /**< valor de resultado */


    /**
     * La declaracion de la variable resultado es la suma de las variables op1 y op2.
     * @param op1 a integer argument.
     * @param op2 a integer argument.
     * return resultado de la suma.
     */
    resultado = op1+op2;
    /**
    * Saca el resultado por pantalla sobre la suma de los 2 numeros.
    */
    printf("El resultado de la suma es %i\n", resultado);

    return 0;
}
